package Actions;

import org.testng.Assert;

import DataObjects.Account;
import Interfaces.LogigearEmail.LoginPage;
import TestData.Constant;
import TestData.Messages;

public class LogigearEmailActions extends GeneralActions {

	public static void login(Account account) {
		System.out.print(Messages.ExeLoginMsg);
		try {
			LoginPage.getTxtUsername().enter(account.getUsername());
			LoginPage.getTxtPassword().enter(account.getPassword());
			LoginPage.getBtnSignIn().click();
			System.out.println(Messages.PassMsg);
		} catch (Exception e) {
			Assert.fail(Messages.FailMsg);
			e.printStackTrace();
		}
	}
	
	public static void openLgMail() {
		openWebPage(Constant.LgMailUrl);
	}
}
