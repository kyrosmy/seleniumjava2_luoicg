package Actions;

import org.openqa.selenium.By;

import TestData.Constant;
import Cores.WebDriverManager;
import Cores.WebElementWrapper;

public class GeneralActions {
	
	public static String getAlertMessage() {
		while (true) {
			long timeOut = System.currentTimeMillis() + Constant.TIMEOUT * 1000;
			try {
				return WebDriverManager.getDriverInstance().getAlert();
			} catch (Exception e) {
				long timeWait = System.currentTimeMillis();
				if (timeWait > timeOut)
					break;
			}
		}
		return WebDriverManager.getDriverInstance().getAlert();
	}
	
	public static boolean checkAlertMessage(String messsage) {
		if (getAlertMessage().trim().equals(messsage)) {
			return true;
		}
		return false;
	}
	
	public static void clickButtonOnAlert(String buttonName) {
		WebDriverManager.getDriverInstance().clickButtonOnAlert(buttonName);
		waitForPageReload();
	}
	
	public static void waitForPageReload() {
		WebDriverManager.getDriverInstance().waitForPageLoaded();
	}
	
	public static void close() {
		WebDriverManager.getDriverInstance().quit();
	}
	
	public static boolean getIfElementExist(By inputXpath) {
		try {
			int count = WebElementWrapper.findElementsWrapper(inputXpath).size();
			if (count > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static void openWebPage(String webUrl) {
		System.out.println("Open web page " + webUrl + " on thread: " + Thread.currentThread().getId());
		WebDriverManager.getDriverInstance().navigate(webUrl);
	}
}
