package TestData;

public class Variables {

	//public static final String DB_URL = "http://192.168.0.106/TADashboard/login.jsp";
	//public static final String DB_URL = "http://172.16.17.23:54000/TADashboard/login.jsp";
	public static final String DB_URL = "http://localhost/TADashboard/login.jsp";
	public static final String HUB_URL = "http://192.168.172.147:4444/wd/hub";
	public static final String RAILWAY_PATH = "http://54.187.158.247:8081/";
	public static final String REPO = "SampleRepository";
	public static final String NEWREPO = "TestRepository";
	public static final String USERNAME = "administrator";
	public static final String PASSWORD = "";
	public static final String USERNAME2 = "mary";
	public static final String PASSWORD2 = "";
	public static final String USERNAME_UPPER = "test";
	public static final String PASSWORD_UPPER = "TEST";
	public static final String USERNAME_SPCHAR = "special";
	public static final String PASSWORD_SPCHAR = "`!@^&*(+_=[{;'";
	public static final String PASSWORD_SPCHAR2 = "special";
	public static final String USERNAME_SPCHAR2 = "@@User";
	public static final String USERNAME_ERROR = "abc";
	public static final String PASSWORD_ERROR = "abc";
	public static final String LOGIN_ERROR_MESSAGE = "Username or password is invalid";
	public static final String MISSING_USERNAME_MESSAGE = "Please enter username!";
}
