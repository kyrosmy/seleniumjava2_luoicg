package TestData;

import java.util.UUID;

public class Utilities {
	public static String getProjectPath() {
		return System.getProperty("user.dir");
	}
	
	// return integer random
	public static int randomInt(int min, int max) {
		// Code in here
		return 0; 
	}
	
	// return character random
    public static char randomCharacter() {
    	// Code in here
        return 'a';
    }

    public static String generateString() {
        String uuid = UUID.randomUUID().toString().replaceAll("-","").substring(15);
        return uuid;
    }
    
    public static void wait(int seconds) {
    	try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static boolean compareTwoStrings(String a, String b, boolean isIgnoreCase)
    {
    	System.out.println(a + "compare:" + b);
        if (isIgnoreCase)
        {
            return a.equalsIgnoreCase(b);
        }

        return a.equals(b);
    }
    
    public static boolean isBlankOrNull(String str) {
        return (str == null || "".equals(str.trim()));
    }
}
