package TestData;

public class Messages {
	
	public static final String WebDriverFailMsg = "Web Driver Failed to Start Up!";
	public static final String ShowThreadIDMsg = "Current Thread ID: ";
	public static final String PreConMsg = "Execute Pre-condition";
	public static final String PostConMsg = "Execute Post-condition";
	
	//Logigear Email TC_01
	public static final String MailBoxTestsTC_01Msg = "TC_01 - Compose and save draft email successfully.";
	public static final String MailBoxTestsTC_01Step1Msg = "Step1 Navigate to https://sgmail.logigear.com/";
	public static final String MailBoxTestsTC_01Step2Msg = "Step2 Log in with your account";
	public static final String MailBoxTestsTC_01Step3Msg = "Step3 Compose new email with attachment";
	public static final String MailBoxTestsTC_01Step4Msg = "Step4 Save the email and close the composing email pop up";
	public static final String MailBoxTestsTC_01VPMsg = "The email is save to Draft folder successfully with correct info(receiver, subject, attachment, content)";
	//
	
	public static final String PassMsg = "Passed";
	public static final String FailMsg = "Failed";
	public static final String ExeLoginMsg = "Login to Logigear Email : ";
}
