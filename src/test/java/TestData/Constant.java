package TestData;

import DataObjects.Account;

public class Constant {

	public static final int TIMEOUT = 30;
	public static final int WebTimeOut = 600;
	public static final String ChromeDriverKey = "Chrome";
	public static final String ChromeDriverRemoteKey = "ChromeRemote";
	public static final String ChromeDriverPath = "\\Executables\\chromedriver.exe";
	
	//Logigear Email Data
	public static final String LgMailUrl = "https://sgmail.logigear.com";
	public static final Account TestAccount = new Account("duong.luong","p@ssw0rd1");	
}
