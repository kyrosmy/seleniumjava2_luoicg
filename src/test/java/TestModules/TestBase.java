package TestModules;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import Cores.WebDriverManager;
import TestData.Constant;
import TestData.Messages;

public class TestBase {
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println(Messages.PreConMsg);
		System.out.println(Messages.ShowThreadIDMsg + Thread.currentThread().getId());
		try {
			//WebDriverManager.startDriver(Constant.ChromeDriverRemoteKey)
			//.getWebDriver().manage().window().maximize();	
			WebDriverManager.startDriver(Constant.ChromeDriverKey)
			.getWebDriver().manage().window().maximize();	
		} catch (Exception e) {
			Assert.fail(Messages.WebDriverFailMsg);
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println(Messages.PostConMsg);
		WebDriverManager.stopDriver();
	}
}
