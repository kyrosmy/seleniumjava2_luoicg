package TestModules.LogigearEmail_TestSuite;

import org.testng.annotations.Test;

import Actions.LogigearEmailActions;
import TestData.Constant;
import TestData.Messages;
import TestModules.TestBase;

public class MailBoxTests extends TestBase{
	
	@Test
	public void TC_01() {
		System.out.println(Messages.MailBoxTestsTC_01Msg);
		
		System.out.println(Messages.MailBoxTestsTC_01Step1Msg);
		LogigearEmailActions.openLgMail();
		
		System.out.println(Messages.MailBoxTestsTC_01Step2Msg);
		LogigearEmailActions.login(Constant.TestAccount);
		
		System.out.println(Messages.MailBoxTestsTC_01Step3Msg);
		
		System.out.println(Messages.MailBoxTestsTC_01Step4Msg);
		
		System.out.println(Messages.MailBoxTestsTC_01VPMsg);
	}
	
}