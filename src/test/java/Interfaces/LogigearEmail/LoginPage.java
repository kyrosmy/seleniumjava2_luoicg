package Interfaces.LogigearEmail;

import org.openqa.selenium.By;

import Cores.WebElementWrapper;

public class LoginPage {

	// Locators
	
	private static final By rdoPublic = By.xpath("//input[@id='rdoPblc']");	
	private static final By rdoPrivate = By.xpath("//input[@id='rdoPrvt']");	
	private static final By txtUsername = By.xpath("//input[@id='username']");
	private static final By txtPassword = By.xpath("//input[@id='password']");
	private static final By btnSignIn = By.xpath("//input[@value='Sign in']");
	
	// Elements

	public static WebElementWrapper getRdoPublic() {
		return WebElementWrapper.findElementWrapper(rdoPublic);
	}
	
	public static WebElementWrapper getRdoPrivate() {
		return WebElementWrapper.findElementWrapper(rdoPrivate);
	}
	
	public static WebElementWrapper getTxtUsername() {
		return WebElementWrapper.findElementWrapper(txtUsername);
	}

	public static WebElementWrapper getTxtPassword() {
		return WebElementWrapper.findElementWrapper(txtPassword);
	}

	public static WebElementWrapper getBtnSignIn() {
		return WebElementWrapper.findElementWrapper(btnSignIn);
	}
}
