package Cores;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import TestData.Constant;

public class WebElementWrapper {

    private final WebElement element;

    public WebElementWrapper(final WebElement inputElement) {
        this.element = inputElement;
    }
    
    public WebElementWrapper getWebElement() {
    	return new WebElementWrapper(element);
    }
    
    public WebElement getRawWebElement() {
    	return element;
    }
    
    public void click() {
    	while (true)
        {    
        	long timeOut = System.currentTimeMillis() + Constant.TIMEOUT*1000;
            try
            {
            	element.click();
            	break;
            }
            catch (Exception e)
            {
            	long timeWait = System.currentTimeMillis();
            	if	(timeWait>timeOut)
            		break;
            }
        }
    }
    
    public boolean isClickable() {
        try {
			element.click();
			return true;
		} catch (Exception e) {
			return false;
		}
    }

    public void sendKeys(CharSequence... keysToSend) {
        try {
			element.sendKeys(keysToSend);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void enter(String input) {
    	try {
			clear();
			sendKeys(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void typeIn(String input) {
    	sendKeys(input);
    }

	public void clear() {
		element.clear();
	}

	public String getTagName() {
		return element.getTagName();
	}

	public String getAttribute(String name) {
		return element.getAttribute(name);
	}

	public boolean isSelected() {
		return element.isSelected();
	}

	public boolean isEnabled() {
		return element.isEnabled();
	}

	public String getText() {
		return element.getText();
	}

	public static List<WebElement> findElements(By by) {
		return WebDriverManager.getDriverInstance().getWebDriver().findElements(by);
	}

	public WebElement findElementDefault(By by) {
		return WebDriverManager.getDriverInstance().getWebDriver().findElement(by);
	}
	
	public static WebElement findElement(By by) {
		int timeout = Constant.TIMEOUT;
		return findElement(by, timeout);
	}
	
	public static WebElementWrapper findElementWrapper(By by) {
		int timeout = Constant.TIMEOUT;
		return new WebElementWrapper(findElement(by, timeout));
	}
	
	public static List<WebElementWrapper> findElementsWrapper(By by) {
		 List<WebElementWrapper>  result = new ArrayList<WebElementWrapper>();
		 List<WebElement> eList = WebDriverManager.getDriverInstance().getWebDriver().findElements(by);
		 for (int i=0; i < eList.size(); i++) {
			 result.add(new WebElementWrapper(eList.get(i)));
		 }
		 return result;
	}
	
	public static WebElement findElement(By by, int timeout) {
		WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriverInstance().getWebDriver(), timeout);
		while (true) {
			long timeOUt = System.currentTimeMillis() + timeout * 1000;
			try {
				return wait.until(ExpectedConditions
						.visibilityOf(wait.until(ExpectedConditions.presenceOfElementLocated(by))));
			} catch (Exception e) {
				// System.out.println(nse);
				long timeWait = System.currentTimeMillis();
				if (timeWait > timeOUt)
					break;

			}
		}
		return null;
	}

	public boolean isDisplayed() {
		return element.isDisplayed();
	}

	public Point getLocation() {
		return element.getLocation();
	}

	public Dimension getSize() {
		return element.getSize();
	}

	public Rectangle getRect() {
		return element.getRect();
	}

	public String getCssValue(String propertyName) {
		return element.getCssValue(propertyName);
	}
	
	public void setCheckboxValue(boolean onOff) {
		if (element.isSelected() != onOff)
			element.click();
	}

	public <File> File getScreenshotAs(OutputType<File> target) throws WebDriverException {
		File screenshot = element.getScreenshotAs(target);
		return screenshot;
	}

	public void submit() {
		element.submit();
	}
	
	public void hover() {
		Actions actionBuilder = new Actions(WebDriverManager.getDriverInstance().getWebDriver()); 
		actionBuilder.moveToElement(element).build().perform();
	}
}
