package Cores;

import java.net.URL;
import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import TestData.Constant;
import TestData.Utilities;
import TestData.Variables;

public class WebDriverManager {

    private static HashMap<Long, WebDriverWrapper> driverMap = new HashMap<>();

    public static WebDriverWrapper getDriverInstance() {
        return driverMap.get(Thread.currentThread().getId());
    }

    public static WebDriverWrapper startDriver(String webBrowser) throws Exception {
        if (webBrowser.equalsIgnoreCase(Constant.ChromeDriverKey)) {
    		System.setProperty("webdriver.chrome.driver", Utilities.getProjectPath() + Constant.ChromeDriverPath);
        	WebDriverWrapper chromeDriver = new WebDriverWrapper(new ChromeDriver());
        	driverMap.put(Thread.currentThread().getId(), chromeDriver);
        } 
        else if (webBrowser.equalsIgnoreCase(Constant.ChromeDriverRemoteKey)) {
        	System.setProperty("webdriver.chrome.driver", Utilities.getProjectPath() + Constant.ChromeDriverPath);
    		DesiredCapabilities desiredCaps = DesiredCapabilities.chrome();
        	WebDriverWrapper chromeDriver = new WebDriverWrapper(new RemoteWebDriver(new URL(Variables.HUB_URL), desiredCaps));
        	driverMap.put(Thread.currentThread().getId(), chromeDriver);
        }
        
        return getDriverInstance();
    }

    public static void stopDriver() {
        WebDriverWrapper driver = driverMap.get(Thread.currentThread().getId());
        driverMap.remove(Thread.currentThread().getId());
        if (null != driver) {
            driver.quit();
            driver.setWebDriver(null);
            driver = null;
        }
    }
}

